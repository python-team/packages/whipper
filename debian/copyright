Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: whipper
Source: https://github.com/whipper-team/whipper/
Upstream-Contact: The Whipper Team

Files: *
Copyright: 2009 Thomas Vander Stichele
           2016-2019 The Whipper Team: JoeLametta, Frederik Olesen,
           Samantha Baldwin, Leo Bogert, Andreas Oberritter,
           Merlijn Wajer, et al.
License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

Files: com.github.whipper_team.Whipper.metainfo.xml
Copyright: 2018 Frederik “Freso” S. Olesen
License: CC0-1.0

Files: debian/*
Copyright: 2017 Krzysztof Krzyżaniak (eloy) <eloy@debian.org>
License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: CC0-1.0
 You may use this work under the terms of a Creative Commons CC0 1.0
 License/Waiver.
 .
 On Debian systems, the complete text of the Creative Commons CC0 1.0
 Universal license can be found in `/usr/share/common-licenses/CC0-1.0'.
